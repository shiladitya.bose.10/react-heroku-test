package com.tvscredit.tvsbackend.dto;

public class VoterIdRequestDto {
    private String consent;
    private String epicNo;
    private ClientData clientData;

    public VoterIdRequestDto(){

    }
    public VoterIdRequestDto(String consent, String epicNo, ClientData clientData) {
        this.consent = consent;
        this.epicNo = epicNo;
        this.clientData = clientData;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public String getEpicNo() {
        return epicNo;
    }

    public void setEpicNo(String epicNo) {
        this.epicNo = epicNo;
    }

    public ClientData getClientData() {
        return clientData;
    }

    public void setClientData(ClientData clientData) {
        this.clientData = clientData;
    }

    public static class ClientData {
        private String caseId;

        public String getCaseId() {
            return caseId;
        }

        public void setCaseId(String caseId) {
            this.caseId = caseId;
        }

        public ClientData() {
        }

        public ClientData(String caseId) {
            this.caseId = caseId;
        }
    }
}