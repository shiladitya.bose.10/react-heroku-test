package com.tvscredit.tvsbackend.dto;

import java.util.List;

public class PinCodeApiResponseDto {
    private String status;
    private List<PinCodeRecordDto> records;




    // getters and setters


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PinCodeRecordDto> getRecords() {
        return records;
    }

    public void setRecords(List<PinCodeRecordDto> records) {
        this.records = records;
    }
}
