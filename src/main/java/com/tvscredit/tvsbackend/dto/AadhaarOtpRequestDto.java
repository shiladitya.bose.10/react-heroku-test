package com.tvscredit.tvsbackend.dto;

public class AadhaarOtpRequestDto {
    private String aadhaarNo;
    private String consent;
    private ClientData clientData;

    public AadhaarOtpRequestDto() {
    }

    public AadhaarOtpRequestDto(String aadhaarNo, String consent, ClientData clientData) {
        this.aadhaarNo = aadhaarNo;
        this.consent = consent;
        this.clientData = clientData;
    }


    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public ClientData getClientData() {
        return clientData;
    }

    public void setClientData(ClientData clientData) {
        this.clientData = clientData;
    }


    public static class ClientData {
        private String caseId;


        public ClientData() {
        }

        public ClientData(String caseId) {
            this.caseId = caseId;
        }


        public String getCaseId() {
            return caseId;
        }

        public void setCaseId(String caseId) {
            this.caseId = caseId;
        }
    }
}

//
// "aadhaarNo": "7**********8",
//         "consent": "Y",
//         "clientData": {
//         "caseId": "123456"
//         }
//         }
