package com.tvscredit.tvsbackend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PanProfileRequestDto {
    private String pan;
    @JsonProperty("PANStatus")
    private String panStatus;
    private String consent;


    public PanProfileRequestDto() {
    }

    public PanProfileRequestDto(String pan, String panStatus, String consent) {
        this.pan = pan;
        this.panStatus = panStatus;
        this.consent = consent;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getPanStatus() {
        return panStatus;
    }

    public void setPanStatus(String panStatus) {
        this.panStatus = panStatus;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }
}