package com.tvscredit.tvsbackend.dto;
import lombok.*;
import org.springframework.stereotype.Component;
@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class ElectricityBillRequestDto {
    private String consent;
    private String consumer_id;
    private String service_provider;
    private ClientData clientData;
    @Data
    public static class ClientData {

        private String caseId;

    }

}