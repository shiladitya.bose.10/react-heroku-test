package com.tvscredit.tvsbackend.service;

import com.tvscredit.tvsbackend.dto.AadhaarOtpRequestDto;

import java.util.Map;

public interface AadharOtpService {
    Map<String, Object> requestOtpOnRegisteredMobileNumber(AadhaarOtpRequestDto aadhaarOtpRequestDto,String endpointString) throws Exception;
}
