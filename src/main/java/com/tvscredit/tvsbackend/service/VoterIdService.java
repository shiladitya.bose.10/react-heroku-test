package com.tvscredit.tvsbackend.service;

import com.tvscredit.tvsbackend.dto.VoterIdRequestDto;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface VoterIdService {
    public Map<String,Object> getVoterIdDetailsFromMule(VoterIdRequestDto voterIdRequestDto,String endpointString) throws Exception;
}
