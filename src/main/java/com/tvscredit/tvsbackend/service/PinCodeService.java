package com.tvscredit.tvsbackend.service;

import com.tvscredit.tvsbackend.dto.PinCodeApiResponseDto;
import org.springframework.http.ResponseEntity;

public interface PinCodeService {
    public ResponseEntity<PinCodeApiResponseDto> fetchDetailsFromGovApi(String reqPincode);
}
