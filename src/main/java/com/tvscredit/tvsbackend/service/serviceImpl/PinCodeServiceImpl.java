package com.tvscredit.tvsbackend.service.serviceImpl;

import com.tvscredit.tvsbackend.dto.PinCodeApiResponseDto;
import com.tvscredit.tvsbackend.exception.PincodeServiceException;
import com.tvscredit.tvsbackend.service.PinCodeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PinCodeServiceImpl implements PinCodeService {

    @Value("${data.gov.pinCode.url}")
    private String apiUrl;

    @Value("${data.gov.pinCode.api.key}")
    private  String apiKey;

    public PinCodeServiceImpl(RestTemplate restTemplate){
        this.restTemplate  = restTemplate;
    }
    private final RestTemplate restTemplate;
    public ResponseEntity<PinCodeApiResponseDto> fetchDetailsFromGovApi(String reqPincode){
           try{
            String url = apiUrl +
                    "?api-key=" + apiKey +
                    "&format=json" +
                    "&limit=1" +
                    "&filters[pincode]="+reqPincode;

               PinCodeApiResponseDto  response = restTemplate.getForEntity(url,PinCodeApiResponseDto.class).getBody();

               return ResponseEntity.status(HttpStatus.OK).body(response);

            } catch (Exception e) {
                   throw new PincodeServiceException("Error while getting Pincode details: " + e.getMessage(), e);
            }
        }

    }



