package com.tvscredit.tvsbackend.service.serviceImpl;
import com.tvscredit.tvsbackend.dto.VoterIdRequestDto;
import com.tvscredit.tvsbackend.exception.VoterIdServiceException;
import com.tvscredit.tvsbackend.service.VoterIdService;
import com.tvscredit.tvsbackend.utils.MulePostRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class VoterIdServiceImpl implements VoterIdService {
    @Autowired
    MulePostRequestHandler mulePostRequestHandler;
    public Map<String, Object> getVoterIdDetailsFromMule(VoterIdRequestDto voterIdRequestDto, String endpointString) throws Exception {
        try {
            return mulePostRequestHandler.requestToMule(voterIdRequestDto,endpointString);
        } catch (Exception e) {
            throw new VoterIdServiceException("Error while getting Voter ID details: " + e.getMessage(), e);
        }
    }
}