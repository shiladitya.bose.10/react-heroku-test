package com.tvscredit.tvsbackend.service.serviceImpl;

import com.tvscredit.tvsbackend.dto.ElectricityBillRequestDto;
import com.tvscredit.tvsbackend.dto.PanProfileRequestDto;
import com.tvscredit.tvsbackend.exception.ElectricityBillServiceException;
import com.tvscredit.tvsbackend.service.ElectricityBillService;
import com.tvscredit.tvsbackend.utils.MulePostRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ElectricityBillServiceImpl implements ElectricityBillService {
    @Autowired
    MulePostRequestHandler mulePostRequestHandler;
    public Map<String,Object> getElectricityBillDetails(ElectricityBillRequestDto electricityBillRequestDto, String endpointString) throws Exception {
        try {
            return mulePostRequestHandler.requestToMule(electricityBillRequestDto, endpointString);
        } catch (Exception e) {
            throw new ElectricityBillServiceException("Error while getting Electricity Bill details " + e.getMessage(), e);
        }


    }
}
