package com.tvscredit.tvsbackend.service.serviceImpl;
import com.tvscredit.tvsbackend.dto.AadhaarOtpRequestDto;
import com.tvscredit.tvsbackend.exception.AadharOtpServiceException;
import com.tvscredit.tvsbackend.exception.ElectricityBillServiceException;
import com.tvscredit.tvsbackend.service.AadharOtpService;
import com.tvscredit.tvsbackend.utils.MulePostRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class AadhaarOtpServiceImpl implements AadharOtpService {
    @Autowired
    MulePostRequestHandler mulePostRequestHandler;
    public Map<String, Object> requestOtpOnRegisteredMobileNumber(AadhaarOtpRequestDto aadhaarOtpRequestDto, String endpointString) throws Exception {
        try {
            return mulePostRequestHandler.requestToMule(aadhaarOtpRequestDto, endpointString);
        } catch (Exception e) {
            throw new AadharOtpServiceException("Error while Requesting OTP from Aadhaar" + e.getMessage(), e);
        }
    }
}