package com.tvscredit.tvsbackend.service.serviceImpl;import com.tvscredit.tvsbackend.dto.PanProfileRequestDto;
import com.tvscredit.tvsbackend.exception.PanProfileServiceException;
import com.tvscredit.tvsbackend.service.PanProfileService;
import com.tvscredit.tvsbackend.utils.MulePostRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class PanProfileServiceImpl implements PanProfileService {
    @Autowired
    MulePostRequestHandler mulePostRequestHandler;
    public Map<String,Object> getPanProfileDetails(PanProfileRequestDto panProfileRequestDto,String endpointString) throws Exception {
        try{
            return mulePostRequestHandler.requestToMule(panProfileRequestDto, endpointString);
        }catch (Exception e){
            throw new PanProfileServiceException("Error while getting PAN Profile details: " + e.getMessage(), e);
        }
    }
}
