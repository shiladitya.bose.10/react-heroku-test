package com.tvscredit.tvsbackend.service;

import com.tvscredit.tvsbackend.dto.PanProfileRequestDto;

import java.util.Map;

public interface PanProfileService {
    public Map<String,Object> getPanProfileDetails(PanProfileRequestDto panProfileRequestDto,String endpointString) throws Exception;
}
