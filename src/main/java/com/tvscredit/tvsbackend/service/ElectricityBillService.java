package com.tvscredit.tvsbackend.service;

import com.tvscredit.tvsbackend.dto.ElectricityBillRequestDto;
import com.tvscredit.tvsbackend.dto.PanProfileRequestDto;

import java.util.Map;

public interface ElectricityBillService {

    public Map<String,Object> getElectricityBillDetails(ElectricityBillRequestDto electricityBillRequestDto, String endpointString) throws Exception;
}
