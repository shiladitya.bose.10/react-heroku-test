package com.tvscredit.tvsbackend.utils;

import com.tvscredit.tvsbackend.dto.AadhaarOtpRequestDto;
import com.tvscredit.tvsbackend.dto.ElectricityBillRequestDto;
import com.tvscredit.tvsbackend.dto.PanProfileRequestDto;
import com.tvscredit.tvsbackend.dto.VoterIdRequestDto;

public class JsonProviderUtil {
    public static String convertObjectToJson(Object requestDto, String endPoint) {
        switch (endPoint) {
            case "aadhaar-otp":
                return convertAadhaarOtpRequestDtoToJson((AadhaarOtpRequestDto) requestDto);
            case "pan-profile":
                return convertPanProfileRequestDtoToJson((PanProfileRequestDto) requestDto);
            case "voter-id-auth":
                return convertVoterIdRequestDtoToJson((VoterIdRequestDto) requestDto);
            case "electricity-bill":
                return convertElectricityBillRequstDtoToJson((ElectricityBillRequestDto) requestDto);
            default:
                throw new IllegalArgumentException("Unsupported Json Converion: " + endPoint);
        }
    }

    private static String convertAadhaarOtpRequestDtoToJson(AadhaarOtpRequestDto aadhaarOtpRequestDto) {
        return "{ \"aadhaarNo\": \"" + aadhaarOtpRequestDto.getAadhaarNo() + "\", " +
                "\"consent\": \"" + aadhaarOtpRequestDto.getConsent() + "\", " +
                "\"clientData\": { \"caseId\": \"" + aadhaarOtpRequestDto.getClientData().getCaseId() + "\" } }";
    }

    private static String convertPanProfileRequestDtoToJson(PanProfileRequestDto panProfileRequestDto) {
        return "{ \"pan\": \"" + panProfileRequestDto.getPan() + "\", " +
                "\"PANStatus\": \"" + panProfileRequestDto.getPanStatus() + "\", " +
                "\"consent\": \"" + panProfileRequestDto.getConsent() + "\" }";
    }

    private static String convertVoterIdRequestDtoToJson(VoterIdRequestDto voterIdRequestDto) {
        return "{ \"consent\": \"" + voterIdRequestDto.getConsent() + "\", " +
                "\"epicNo\": \"" + voterIdRequestDto.getEpicNo() + "\", " +
                "\"clientData\": { \"caseId\": \"" + voterIdRequestDto.getClientData().getCaseId() + "\" } }";
    }

    private static String convertElectricityBillRequstDtoToJson (ElectricityBillRequestDto electricityBillRequestDto) {
        return "{ \"consumer_id\": \"" + ( electricityBillRequestDto).getConsumer_id() + "\", " +
                "\"service_provider\": \"" + ( electricityBillRequestDto).getService_provider() + "\", " +
                "\"consent\": \"" + ( electricityBillRequestDto).getConsent() + "\","+
                "\"clientData\": { \"caseId\": \"" + electricityBillRequestDto.getClientData().getCaseId() + "\" } }";
    }

}
