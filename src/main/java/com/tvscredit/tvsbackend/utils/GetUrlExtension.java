package com.tvscredit.tvsbackend.utils;

public class GetUrlExtension {
    public static String getExtension(String endpointString){
        switch (endpointString){
            case "pan-profile":
                return "/api/pan-profile";
            case "voter-id-auth":
                return  "/api/voter";
            case "aadhaar-otp":
                return  "/api/otp";
            case "electricity-bill":
                return "/api/elec";
            default:
                throw new IllegalArgumentException("Url Not Found");
        }
    }
}
