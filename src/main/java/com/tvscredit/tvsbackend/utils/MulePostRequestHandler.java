package com.tvscredit.tvsbackend.utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component
public class MulePostRequestHandler {
    private static final Logger logger = LoggerFactory.getLogger(MulePostRequestHandler.class);
    @Value("${mule.url}")
    private String muleUrl;

    @Value("${mule.clientId}")
    private  String clientId;

    @Value("${mule.clientSecret}")
    private String clientSecret;


    private  final  RestTemplate restTemplate;

    public  MulePostRequestHandler(RestTemplate restTemplate){

        this.restTemplate = restTemplate;
    }



    public Map<String,Object> requestToMule(Object dtoObject, String endpointString) throws Exception  {
        Map<String,Object> response = new HashMap<>();
        try{
            String fullUrl = muleUrl + GetUrlExtension.getExtension(endpointString);
            logger.info("Full Url - "+fullUrl);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("client_ID", clientId);
            headers.set("client_secret", clientSecret);

            String requestBody = JsonProviderUtil.convertObjectToJson(dtoObject,endpointString);
            logger.info("Request Body - "+requestBody);


            HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);
            ResponseEntity<Object> responseEntity = restTemplate.exchange(
                    fullUrl,
                    HttpMethod.POST,
                    requestEntity,
                    Object.class
            );

            response.put("Success","true");
            response.put("response",responseEntity.getBody());
            logger.info("Response - "+response);
             return response;


        }catch (Exception e){
//            logger.error(e.getMessage());
//            ResponseEntity<Object> errorObject = ResponseEntity.internalServerError().body("Please Check Provided Details");
//            response.put("Error",errorObject);
            throw new Exception(e);
        }

    }



}
