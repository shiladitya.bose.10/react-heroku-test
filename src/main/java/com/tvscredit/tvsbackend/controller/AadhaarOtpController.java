package com.tvscredit.tvsbackend.controller;

import com.tvscredit.tvsbackend.dto.AadhaarOtpRequestDto;
import com.tvscredit.tvsbackend.service.AadharOtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Map;
@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class AadhaarOtpController {
    @Autowired
    public AadharOtpService aadharOtpService;
    @PostMapping("aadhaar-otp")
    public Map<String,Object> requestOtpFromRegisteredMobileNumber(@RequestBody AadhaarOtpRequestDto aadhaarOtpRequestDto, HttpServletRequest request) throws Exception {
        String uri = request.getRequestURI();
        String endpointString =  uri.substring(uri.lastIndexOf("/") + 1);
        return aadharOtpService.requestOtpOnRegisteredMobileNumber(aadhaarOtpRequestDto,endpointString);
    }
}
