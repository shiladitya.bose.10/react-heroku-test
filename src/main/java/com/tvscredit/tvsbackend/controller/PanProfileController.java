package com.tvscredit.tvsbackend.controller;
import com.tvscredit.tvsbackend.dto.PanProfileRequestDto;
import com.tvscredit.tvsbackend.service.PanProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Map;
@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class PanProfileController {

    @Autowired
    PanProfileService panProfileService;
    @PostMapping("/pan-profile")
    public Map<String,Object> getPanProfileDetails(@RequestBody PanProfileRequestDto panProfileRequestDto, HttpServletRequest request) throws Exception  {
        String uri = request.getRequestURI();
        String endpointString =  uri.substring(uri.lastIndexOf("/") + 1);
        return panProfileService.getPanProfileDetails(panProfileRequestDto,endpointString);
    }
}
