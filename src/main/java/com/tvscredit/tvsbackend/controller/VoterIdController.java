package com.tvscredit.tvsbackend.controller;

import com.tvscredit.tvsbackend.dto.VoterIdRequestDto;
import com.tvscredit.tvsbackend.service.VoterIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Map;
@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class VoterIdController {

    @Autowired
    VoterIdService voterIdService;
    @PostMapping("/voter-id-auth")
    public Map<String, Object> getVoterIdDetails(@RequestBody VoterIdRequestDto voterIdRequestDto, HttpServletRequest request) throws Exception {
        String uri = request.getRequestURI();
        String endpointString =  uri.substring(uri.lastIndexOf("/") + 1);
        return voterIdService.getVoterIdDetailsFromMule(voterIdRequestDto,endpointString);
    }
}
