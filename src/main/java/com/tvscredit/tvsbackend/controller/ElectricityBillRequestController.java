package com.tvscredit.tvsbackend.controller;

import com.tvscredit.tvsbackend.dto.AadhaarOtpRequestDto;
import com.tvscredit.tvsbackend.dto.ElectricityBillRequestDto;
import com.tvscredit.tvsbackend.service.AadharOtpService;
import com.tvscredit.tvsbackend.service.ElectricityBillService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class ElectricityBillRequestController {
    @Autowired
    public ElectricityBillService electricityBillService;
    @PostMapping("electricity-bill")
    public Map<String,Object> getElectricityBill(@RequestBody ElectricityBillRequestDto electricityBillRequestDto, HttpServletRequest request) throws Exception {
        String uri = request.getRequestURI();
        String endpointString =  uri.substring(uri.lastIndexOf("/") + 1);
        return electricityBillService.getElectricityBillDetails(electricityBillRequestDto,endpointString);
    }
}
