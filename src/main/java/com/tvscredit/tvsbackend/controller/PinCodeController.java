package com.tvscredit.tvsbackend.controller;

import com.tvscredit.tvsbackend.dto.PinCodeApiResponseDto;
import com.tvscredit.tvsbackend.service.PinCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class PinCodeController {
    @Autowired
    private PinCodeService pinCodeService ;
    @GetMapping("/pincode/{pinCode}")
    public ResponseEntity<PinCodeApiResponseDto> fetchDetailsFromGovApi(@PathVariable("pinCode") String reqPincode){
        ResponseEntity<PinCodeApiResponseDto> pinCodeApiResponseDtoResponseEntity = pinCodeService.fetchDetailsFromGovApi(reqPincode);
        return pinCodeApiResponseDtoResponseEntity;
    }
}
