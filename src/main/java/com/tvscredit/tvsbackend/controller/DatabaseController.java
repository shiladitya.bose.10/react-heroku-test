package com.tvscredit.tvsbackend.controller;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@SpringBootApplication
@Controller
public class DatabaseController {

    @GetMapping("/")
    public String index(){
        return "index";
    }
}
