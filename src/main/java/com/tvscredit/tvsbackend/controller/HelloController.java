package com.tvscredit.tvsbackend.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "http://localhost:3000",allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class HelloController {

    @GetMapping("/hello-there")
    public String hello(){
        return "Hi, backend is working";
    }
}
