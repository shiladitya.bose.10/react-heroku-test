package com.tvscredit.tvsbackend.exception.handler;
import com.tvscredit.tvsbackend.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(AadharOtpServiceException.class)
    public ResponseEntity<String> handleAadharOtpServiceException(AadharOtpServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(ElectricityBillServiceException.class)
    public ResponseEntity<String> handleElectricityBillServiceException(ElectricityBillServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(PanProfileServiceException.class)
    public ResponseEntity<String> handlePanProfileServiceException(PanProfileServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(PincodeServiceException.class)
    public ResponseEntity<String> handlePincodeServiceException(PincodeServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(VoterIdServiceException.class)
    public ResponseEntity<String> handleVoterIdServiceException(VoterIdServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
