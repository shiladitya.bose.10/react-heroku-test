package com.tvscredit.tvsbackend.exception;

public class VoterIdServiceException extends RuntimeException {
    public VoterIdServiceException(String message,Throwable cause) {
        super(message,cause);
    }
}
