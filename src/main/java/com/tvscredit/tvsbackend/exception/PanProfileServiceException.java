package com.tvscredit.tvsbackend.exception;

public class PanProfileServiceException extends RuntimeException {
    public PanProfileServiceException(String message,Throwable cause) {
        super(message,cause);
    }
}
