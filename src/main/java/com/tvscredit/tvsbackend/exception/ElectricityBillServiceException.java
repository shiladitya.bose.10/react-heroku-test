package com.tvscredit.tvsbackend.exception;

public class ElectricityBillServiceException extends RuntimeException {
    public ElectricityBillServiceException(String message,Throwable cause) {
        super(message,cause);
    }
}