package com.tvscredit.tvsbackend.exception;

public class AadharOtpServiceException extends RuntimeException {
    public AadharOtpServiceException(String message, Throwable cause) {
        super(message,cause);
    }
}
