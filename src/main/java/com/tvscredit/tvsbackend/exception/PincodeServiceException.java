package com.tvscredit.tvsbackend.exception;

public class PincodeServiceException extends RuntimeException {
    public PincodeServiceException(String message,Throwable cause) {
        super(message,cause);
    }
}