package com.tvscredit.tvsbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvsCreditBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvsCreditBackendApplication.class, args);
	}

}
